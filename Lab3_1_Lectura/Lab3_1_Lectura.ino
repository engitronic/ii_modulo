/* Este programa nos permitirá leer voltaje de un potenciómetro, 
a través de un pin analógico y imprimirlo en el puerto serie */
float valor;
float voltaje;
void setup(){
  Serial.begin(9600);         //Establece la velocidad de comunicación
}
void loop(){
  valor = analogRead(A0);   //Guarda la lectura (0-1023)
  voltaje = valor*5/1023;   //Guarda la conversión a voltaje (0-5V)
  Serial.print("El Voltaje en el potenciometro es: ");
  Serial.print(valor);          //Imprime en el puerto serie “valor”
  Serial.print(" y equivale a ");
  Serial.println(voltaje);  //Imprime en el puerto serie “voltaje”
  delay(500);
}

