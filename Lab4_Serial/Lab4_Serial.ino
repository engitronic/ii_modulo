char dato;            

void setup(){

  pinMode(8,OUTPUT);   // Pin 8 como Salida
  pinMode(9,OUTPUT);   // Pin 9 como Salida
  pinMode(10,OUTPUT);  // Pin 10 como Salida
  Serial.begin(9600);  // Abre el puerto
}

void loop(){
  if (Serial.available()>0){  // Verifica si hay dato disponible
    dato = Serial.read();      // Lee el puerto
    if (dato=='A'){
      digitalWrite(8,HIGH);  // Si es A, PIN8 = HIGH
    }
    if (dato=='B'){
      digitalWrite(9,HIGH);  // Si es B, PIN9 = HIGH
    }
    if (dato=='C'){
      digitalWrite(10,HIGH); // Si es C, PIN10 = HIGH
    }
    if (dato=='D'){
      digitalWrite(8,LOW);   // Si es D, PIN8 = LOW
    }
    if (dato=='E'){
      digitalWrite(9,LOW);   // Si es E, PIN9 = LOW
    }
    if (dato=='F'){
      digitalWrite(10,LOW);  // Si es F, PIN10 = LOW
    }
  }
}
