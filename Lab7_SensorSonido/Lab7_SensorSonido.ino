/* Este es un código que nos permitirá prender un led con un chasquido y apagarlo con dos chasquidos */

#include <TimerOne.h>  // Librería para INTERRUPCION TIMER
int Led = 13;          // Pin del LED
int buttonpin = 3;     // Pin de la ENTRADA DIGITAL
int val = 0;           // Estado variable de la lectura
int aplausos=0;        // Contador de aplausos
void setup (){  
  Serial.begin(9600);    // Inicia el Serial,
  pinMode (Led, OUTPUT) ;       // Como SALIDA
  pinMode (buttonpin, INPUT) ;      // Como entrada 
  Timer1.initialize(1300000);       // Cada 1300 ms la interrupción
  Timer1.attachInterrupt(APLAUSOS); // Activa la interrupción
}
void loop (){
  val = digitalRead(buttonpin);  // Carga el estado leído
  if (val == HIGH){              // ¿Senso?
  aplausos++;                 // Incrementa contador "aplauso"
  delay(60);                  // Pausa de 60 ms
  }
}
void APLAUSOS(){   
  // El estado de "aplausos" es visualizable en el MONITOR SERIAL
  Serial.println(aplausos);  
  if (aplausos == 1){     // ¿Un aplauso?
  digitalWrite (Led, HIGH);  
  }
  if (aplausos > 1){      // ¿Dos aplausos?
  digitalWrite (Led, LOW);    
  }    
  aplausos=0;             // Reinicia contador "aplausos"
}

