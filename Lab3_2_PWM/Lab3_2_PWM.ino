/*Este programa nos permitirá variar la intensidad de un led 
dependiendo la lectura que obtengamos de un potenciómetro */
int led = 9;             
int valor;                // Variable para almacenar lectura
void setup(){
  pinMode(led,OUTPUT);      // Configura el pin como salida
}
void loop(){
  valor = analogRead(A0);     // Guarda la lectura del potenciometro
  analogWrite(led, valor/4);  // Hace variar la intensidad del led.
}

