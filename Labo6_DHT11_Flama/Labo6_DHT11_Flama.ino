// Este es un código que nos permitirá activar un ventilador y buuzer cuando detecte presencia de flama.

#include <dht11.h>     // Librería DHT11
dht11 TEMPE;           // Creamos el objeto TEMPE
#define DHT_out 7      // Asignamos el pin 7 como salida del DHT11 

int ventilador=8;
int buzzer=9;
int lectura;
int temperatura;
int flama;
void setup(){
  Serial.begin(9600);
  pinMode(8,OUTPUT);
  pinMode(9,OUTPUT);
}
void loop(){
  lectura = TEMPE.read(DHT_out);    // Leemos los datos del sensor 
  flama = analogRead(A0);           // Leemos los datos del sensor
  temperatura = TEMPE.temperature;  // Guardamos la temperatura

  if(flama > 950){                 // Se pone como rango 950 
     digitalWrite(ventilador,HIGH);// mayor se activa el ventilador 
     digitalWrite(buzzer,HIGH);    // mayor se activa el buzzer
     Serial.println("Buzzer Prendido ");
  }
  else{             // Si es menor se apagan ambos
     digitalWrite(ventilador,LOW);    
     digitalWrite(buzzer,LOW); 
     Serial.println("Buzzer Apagado ");

  }
  Serial.print(" Temperatura: "); // Imprime la temperatura
  Serial.print(temperatura);
  Serial.println(" C"); 
  delay(500);                     // Pequeño retardo 
}

