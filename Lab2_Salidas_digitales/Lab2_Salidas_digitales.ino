/* Este programa nos permitirá parpadear un led en 
intervalos de 500 milisegundos */
int led = 8;                    // Crea una variable local de valor 8
void setup(){
  pinMode(led,OUTPUT);         // Configura el pin 8 como salida.
}
void loop(){
  digitalWrite(led,HIGH);      // Se prende el led
  delay(500);                 // Tiempo de espera de 500 ms 
  digitalWrite(led,HIGH);      // Se apaga el led
  delay(500);                 // Tiempo de espera de 500 ms
}

