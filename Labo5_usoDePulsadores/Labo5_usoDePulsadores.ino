/* Este programa nos controlar un motor y un led mediante 2 pulsadores que se usan como sensores */

int pulMotor;    // Declarar la variable PulMotor
int pulLed;     // Declarar la variable PulLed

void setup() {
  //Entrada (Configurado por defecto como   HIGH)
  pinMode(13, INPUT_PULLUP); 
  //Salida para el motor
  pinMode(12, OUTPUT);
  //Entrada (Configurado por defecto como HIGH)
  pinMode(2, INPUT_PULLUP);
  //Salida para el LED
  pinMode(8, OUTPUT);
}

void loop() {
  pulMotor = digitalRead(13);    //Lectura de pulsador de Motor
  digitalWrite(12, pulMotor);   //Activa o Desactiva el Motor
 
  pulLed = digitalRead(2);       //Lectura de pulsador de LED
  digitalWrite(8, pulLed);      //Activa o Desactiva el LED
 
  delay(50);                     //Espera 50 milisegundos
}

